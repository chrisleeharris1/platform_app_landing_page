const intitialState = {
  currentUser: null,
  loading: false
};

export function reducer(state = intitialState, action) {
  const { type, payload } = action;

  switch (type) {
    case 'POST_USER_SUCCESS': {
      return {
        ...state,
        currentUser: payload,
      };
    }
    case 'UPDATE_USER_SUCCESS': {
      return {
        ...state,
        currentUser: payload,
      };
    }
    case 'LOGIN_USER_SUCCESS': {
      return {
        ...state,
        currentUser: payload,
      };
    }
    case 'LOGOUT_USER_SUCCESS': {
      return {
        ...state,
        currentUser: payload,
      };
    }
    case 'SHOW_LOADER': {
      return {
        ...state,
        loading: true,
      };
    }
    case 'HIDE_LOADER': {
      return {
        ...state,
        loading: false,
      };
    }
    default:
      return state;
  }
}
