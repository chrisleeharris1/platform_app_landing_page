import React, { useState } from 'react';
import { connect } from 'react-redux';
import { navigate } from 'gatsby';
import 'antd/dist/antd.css';
import './index.css';
import {
  Form,
  Input,
  InputNumber,
  Tooltip,
  Cascader,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  AutoComplete,
} from 'antd';
import ContactFromWrapper, { SectionMainWrapper } from './contact.style';
import PropTypes from 'prop-types';
import Box from 'common/src/components/Box';
import Text from 'common/src/components/Text';
import Heading from 'common/src/components/Heading';
import Container from 'common/src/components/UI/Container';
import FeatureBlock from 'common/src/components/FeatureBlock';
import { updateUserInfo } from '../../../actions';

const { Option } = Select;
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};



const EthnicityRaceGenderDropdownSection = ({
  sectionWrapper,
  row,
  contactForm,
  secTitleWrapper,
  button,
  note,
  title,
  description,
  currentUser,
  updateUserInfo,
  userToken,
  showLoader,
  hideLoader,
  loading
}) => {
  const [form] = Form.useForm();


  const onFinish = async (values) => {
    values["applicationStep"] = '/moreUserInfo/';

    console.log('onFinish values ', values);

    userToken = JSON.parse(localStorage.getItem('jwt'));
    console.log('More User info: userToken', userToken);
    console.log("values.gender.value", values.gender)
    currentUser.gender = values.gender;
    console.log("current user gender", currentUser.gender)
    currentUser.ethnicity = values.ethnicity;
    currentUser.race = values.race;
    currentUser.applicationStep = '/finalApplicationCheck/';
    console.log('currentUser with ownership updates', currentUser);

    //showLoader();
    updateUserInfo(currentUser, userToken);
  
    //hideLoader();
   
    navigate('/finalApplicationCheck/');
  };


  return (
    <SectionMainWrapper>

      <Box {...sectionWrapper}>
        <Container className="containerClass">
          <Box {...secTitleWrapper}>
            <FeatureBlock
              title={<Heading {...title} />}
              description={<Text {...description} />}
            />
          </Box>
          <Box {...row}>
            <Box {...contactForm}>
              <ContactFromWrapper>
              
               
    <Form
      //{...formItemLayout}
      form={form}
      layout="vertical"
      name="register"
      onFinish={onFinish}
      scrollToFirstError
      style={{width:'60%'}}
    >      

      <Form.Item
        name="gender"
        label="Gender"
        rules={[
          {
            required: true,
            message: '*Required',
          },
        ]}
      >
        <Select
          placeholder="Select"
          allowClear
        >
          <Option value="MALE">male</Option>
          <Option value="FEMALE">female</Option>
        </Select>
      </Form.Item>

      <Form.Item
        name="ethnicity"
        label="Ethnicity"
        rules={[
          {
            required: true,
            message: '*Required',
          },
        ]}
      >
        <Select
          placeholder="Select"
          allowClear
        >
          <Option value="LATINO">latino</Option>
          <Option value="NON-LATINO">non-latino</Option>
        </Select>
      </Form.Item>


      <Form.Item
        name="race"
        label="Race"
        rules={[
          {
            required: true,
            message: '*Required',
          },
        ]}
      >
        <Select
          placeholder="Select"
          allowClear
        >
          <Option value="AMERICAN INDIAN OR ALASKAN NATIVE">American Indian or Alaskan Native</Option>
          <Option value="ASIAN">Asian</Option>
          <Option value="BLACK OR AFRICAN AMERICAN">Black or African American</Option>
          <Option value="NATIVE AMERICAN OR OTHER PACIFIC ISLANDER">Native Hawaiian or Other Pacific Islander</Option>
          <Option value="WHITE">White</Option>

        </Select>
      </Form.Item>

      <Form.Item {...tailFormItemLayout}>
        <Button type="primary" htmlType="submit"
          {...button}
          >
          Continue
          
        </Button>
      </Form.Item>
    </Form>   
    
    </ContactFromWrapper>
            </Box>
          </Box>
        </Container>
      </Box>
    
    </SectionMainWrapper>
  );
};

EthnicityRaceGenderDropdownSection.propTypes = {
  sectionWrapper: PropTypes.object,
  secTitleWrapper: PropTypes.object,
  row: PropTypes.object,
  contactForm: PropTypes.object,
  secHeading: PropTypes.object,
  secText: PropTypes.object,
  button: PropTypes.object,
  note: PropTypes.object,
  title: PropTypes.object,
  description: PropTypes.object,
  colornote: PropTypes.object,
};

EthnicityRaceGenderDropdownSection.defaultProps = {
  sectionWrapper: {
    id: 'contact_section',
    as: 'section',
    pt: ['8px', '80px', '80px', '80px'],
    pb: ['0', '80px', '80px', '80px', '80px'],
  },
  secTitleWrapper: {
    mb: ['40px', '40px', '40px'],
    p: ['0 15px', 0, 0, 0, 0],
  },
  secText: {
    as: 'span',
    display: 'block',
    textAlign: 'center',
    fontSize: `${2}`,
    letterSpacing: '0.15em',
    fontWeight: `${6}`,
    color: 'primary',
    mb: `${3}`,
  },
  secHeading: {
    textAlign: 'center',
    fontSize: [`${6}`, `${8}`],
    fontWeight: '400',
    color: 'headingColor',
    letterSpacing: '-0.025em',
    mb: `${0}`,
  },
  row: {
    flexBox: true,
    justifyContent: 'center',
  },
  contactForm: {
    width: [1, 1, 1, 1 / 2],
  },
  button: {
    type: 'button',
    fontSize: `${2}`,
    fontWeight: '600',
    //borderRadius: '4px',
    pl: '22px',
    pr: '22px',
    colors: 'primaryWithBg',
    height: `${4}`,
  },
  note: {
    fontSize: '16px',
    fontWeight: '400',
    color: '#525f7f',
    lineHeight: '28px',
    mb: ['25px', '25px', '30px', '30px', '45px'],
    textAlign: ['center', 'center'],
  },
  colornote: {
    fontSize: '16px',
    fontWeight: '500',
    color: 'rgb(106, 82, 253)',
    lineHeight: '28px',
    mb: ['25px', '25px', '30px', '30px', '45px'],
    textAlign: ['center', 'center'],
  },
  title: {
    content: "Just a few more things until you're approved!",
    fontSize: ['20px', '26px', '30px', '36px', '40px'],
    lineHeight: ['30px', '32px', '40px', '50px', '55px'],
    fontWeight: '700',
    color: '#32325d',
    letterSpacing: '-0.010em',
    mb: '20px',
    textAlign: ['center', 'center'],
  },

  description: {
    content: 'The federal government requests the below information to prevent discrimination. You are not required to provide the below information and it will not impact your application, approval, or rate.',
    fontSize: '16px',
    fontWeight: '400',
    color: '#525f7f',
    lineHeight: '28px',
    mb: ['25px', '25px', '30px', '30px', '45px'],
    textAlign: ['center', 'center'],
  },
};

const mapStateToProps = (state) => ({
    currentUser: state.root.currentUser,
  });
  
  const mapDispatchToProps = (dispatch) => {
    return {
      updateUserInfo: (currentUser, userToken) =>
        dispatch(updateUserInfo(currentUser, userToken)),
    };
  };
  
  const AntEthnicityRaceGenderDropdownSectionRedux = connect(
    mapStateToProps,
    mapDispatchToProps
  )(EthnicityRaceGenderDropdownSection);
  
  export default AntEthnicityRaceGenderDropdownSectionRedux;