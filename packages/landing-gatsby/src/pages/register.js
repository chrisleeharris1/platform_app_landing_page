import React, { Fragment } from 'react';
import { Provider } from 'react-redux';
import Sticky from 'react-stickynode';
import { ThemeProvider } from 'styled-components';
import { saasTheme } from 'common/src/theme/saas';
import { ResetCSS } from 'common/src/assets/css/style';
import { GlobalStyle, ContentWrapper } from '../containers/Saas/saas.style';
import { DrawerProvider } from 'common/src/contexts/DrawerContext';
import Navbar from '../containers/Saas/Navbar';
import BannerSectionNoWords from '../containers/Saas/BannerSectionNoWords';
import SEO from '../components/seo';
import store from '../store';

import RegistrationForm from '../containers/Saas/RegistrationForm';


const ApplicationForm = () => {
  return (
    <ThemeProvider theme={saasTheme}>
      <Provider store={store}>
        <Fragment>
          <SEO title="Improve | Simple, Online, Fast" />
          <ResetCSS />
          <GlobalStyle />
          <ContentWrapper>
            <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
              <DrawerProvider>
                <Navbar />
              </DrawerProvider>
            </Sticky>
            <BannerSectionNoWords />

            
           
            <p>
              
            </p>
            <RegistrationForm />
            <p>

            </p>
            
            
          </ContentWrapper>
        </Fragment>
      </Provider>
    </ThemeProvider>
  );
};

export default ApplicationForm;


