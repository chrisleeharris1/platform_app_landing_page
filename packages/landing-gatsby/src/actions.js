//const FileType = require('file-type');
//import fs from 'fs'
//import FileType from "file-type"

export const createUsers = (formValues) => (dispatch) => {
  return fetch(
    'http://localhost:3000/users',
    //`https://juno-back-end.herokuapp.com/users`,
    {
      method: 'POST',
     // mode: 'cors',

      headers: {
        'Content-Type': 'application/json',
        },
      body: JSON.stringify(formValues),
    }
  )
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log('application submission response');
      console.log(data);
      //save token to local storage as its own variable so as not to get overwritten
      console.log('application submission response token', data.token);
      const serializedToken = JSON.stringify(data.token);
      localStorage.setItem('jwt', serializedToken);

      dispatch({
        type: 'POST_USER_SUCCESS',
        payload: data,
      });
    });
};

export const getUser = (userId) => async (dispatch) => {
  // const response = await fetch();
  
  // dispatch({
  //   type: 'GET_USER_SUCCESS',
  //   payload: response.data,
  // });
};

export const updateUserInfo = (valuesToUpdate, token) => (dispatch) => {
  console.log('Action updateUserInfo: token', token);
  console.log('Action updateUserInfo: values input', valuesToUpdate);
  fetch(
    'http://localhost:3000/users/me',
    //`https://juno-back-end.herokuapp.com/users/me`,
    {
      method: 'PATCH',
      mode: 'cors',

      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(valuesToUpdate),
    }
  )
    .then((response) => {
      console.log('Action response: updateUserInfo', response);
      return response.json();
    })
    .then((data) => {
      //console.log('Action response: updateUserInfo', data);
      dispatch({
        type: 'UPDATE_USER_SUCCESS',
        payload: data,
      });
    });
};

export const loginUser = (formValues) => (dispatch) => {
  console.log("in login user action")
  fetch(
    'http://localhost:3000/users/login',
    //`https://juno-back-end.herokuapp.com/users/login`,
    {
      method: 'POST',
      mode: 'cors',

      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formValues),
    }
  )
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log('redux login response');
      console.log(data);
      //save token to local storage as its own variable so as not to get overwritten
      console.log('redux login response token', data.token);
      const serializedToken = JSON.stringify(data.token);
      localStorage.setItem('jwt', serializedToken);
      dispatch({
        type: 'LOGIN_USER_SUCCESS',
        payload: data,
      });
    }).catch(err => {
        console.log(err)
    })
};

export const logoutUser = (currentUser) => (dispatch) => {
  console.log("in login user action")
  fetch(
    //'http://localhost:3000/users/logout',
    `https://juno-back-end.herokuapp.com/users/logout`,
    {
      method: 'POST',
      mode: 'cors',

      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(currentUser),
    }
  )
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log('redux logout response');
      console.log(data);
      //save token to local storage as its own variable so as not to get overwritten
    
      dispatch({
        type: 'LOGOUT_USER_SUCCESS',
        payload: data,
      });
    }).catch(err => {
        console.log(err)
    })
};

export const sendFile = (file, token, route) => (dispatch) => {
  //console.log("filetype in send file", FileType.fromFile(file));
  console.log('action: sendFile: token', token);
  console.log('action: sendFile: file', file);
  console.log("action: sendFile: route", route)
  const formData = new FormData();
  formData.append(route, file);
  console.log("action: sendFile file converted to formData", formData)
  fetch(
    `http://localhost:3000/users/me/${route}`,
    //`https://juno-back-end.herokuapp.com/users/me/${route}`,
    {
      method: 'POST',
      mode: 'cors',

      headers: {
        //'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: formData
    }
  )
    .then((response) => {
      console.log('response from back end from update user', response);
      return response.json();
    })
    .then((data) => {
      console.log('update User action response data', data);
      dispatch({
        type: 'UPDATE_USER_SUCCESS',
        payload: data,
      });
    });
};


export const showLoader = () => dispatch => {
  console.log("show loader action")
  dispatch({
    type: "SHOW_LOADER"
  })
}

export const hideLoader = () => dispatch => {
  console.log("hide loader action")
  dispatch({
    type: "HIDE_LOADER"
  })
}